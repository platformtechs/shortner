import { Router } from 'express';
import * as UrlController from './controller';
import checkAuth from '../../config/checkAuth';

const routes = new Router();

routes.post('/u', checkAuth, UrlController.createUrl);
routes.get("/u/number", UrlController.urlStatics);
routes.get('/u/:slug', UrlController.redirectUrl);

export default routes;
