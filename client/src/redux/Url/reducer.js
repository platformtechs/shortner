import { LOGIN, GET_USER} from './contants';

const initialState = {
    isLoggedIn : true,
    user: {},
    token: null
}

export default (state= initialState, {type, payload}) => {
    switch(type){
        case  LOGIN: {
            let isLoggedIn = true;
            return {
                ...state,
                isLoggedIn,
                user: payload.user,
                token: payload.token
            }
        }

        case GET_USER: {
            return {
                ...state,
                user: payload
            }
        }

        default: {
            return {
                ...state
            }
        }
    }
}

