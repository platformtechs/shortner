import mongoose from 'mongoose';
import bcrypt from 'bcrypt';
import jwt from 'jsonwebtoken';
import User from './model';

const signToken = (id, password, role) => jwt.sign({ id, password, role }, process.env.JWT_KEY, { expiresIn: '1h' });

export const createAdmin = async (req, res) => {
  try {
    const { fullname, email, password } = req.body;
    const oldUser = await User.findOne({ email });
    if(oldUser){
      return res.status(403).json({ error: true, message: 'user already exist' });
    }
    const hash = await bcrypt.hash(password, 7);    
    const newUser = new User({
      _id: new mongoose.Types.ObjectId(),
      fullname,
      email,
      password: hash,
      role: 'ADMIN',
    });
    const token = signToken(newUser._id, hash, newUser.role);
    return res.status(200).json({ error: false, message: 'user created', data: await newUser.save(), token });  
  } catch (e) {
    console.log(e);
    return res.status(500).json({ error: true, message: e.message });
  }
};

export const createUser = async (req, res) => {
  try {
    const { fullname, email, password } = req.body;
    const oldUser = await User.findOne({ email });
    if (oldUser) {
      return res.status(403).json({ error: true, message: 'user already exist' });
    }
    const hash = await bcrypt.hash(password, 7);
    const newUser = new User({
      _id: new mongoose.Types.ObjectId(),
      fullname,
      email,
      password: hash,
      role: 'USER',
    });
    const token = signToken(newUser._id, hash, newUser.role);
    return res.status(200).json({ error: false, message: 'user created', data: await newUser.save(), token });
  } catch (e) {
    console.log(e);
    return res.status(500).json({ error: true, message: e.message });
  }
};

export const login = async (req, res) => {
  try {
    const { email, password } = req.body;
    const user = await User.findOne({ email });
    if (!user) {
      return res.status(404).json({ error: true, message: 'Invalid email or Password' });
    }
    const match = await bcrypt.compare(password, user.password);
    if(!match){
      return res.status(404).json({ error: true, message: 'Invalid email or Password' });
    }
    const token = signToken(user._id, user.password, user.role);
    return res.status(200).json({ error: false, message: 'user authenticated', data: user, token });
  } catch (e) {
    console.log(e);
    return res.status(500).json({ error: true, message: e.message });
  }
};

export const getUser = async (req, res) => {
  try {
    const userId = mongoose.Types.ObjectId(req.params.id);
    return res.status(201).json({ error: false,
      message: 'user found',
      data: await User.findById(userId).populate('urls')})
  } catch (e) {
    console.log("error", e)
    return res.status(500).json({ error: true, message: e.message });
  }
};

export const getlistUser = async (req, res) => {
  try {
    const { start } = req.query;
    let result = [];
    if(start){
      result = await User.find({ _id: { $gt: start } }).limit(10);
    } else {
      result = await User.find().limit(10);
    }
    const [next] = result.slice(-1);
    return res.status(200).json({ error: false, message: 'User list found', data: result, hasNext: !!next, next: next ? next._id : null });
  } catch (e) {
    return res.status(500).json({ error: true, message: e.message });
  }
};

export const deleteUser = async (req, res) => {
  try {
    const userId = mongoose.Types.ObjectId(req.params.id);
    const result = await User.findByIdAndDelete(userId);
    return res.status(200).json({ error: false, message: 'User deleted', data: result });
  } catch (e) {
    return res.status(500).json({ error: true, message: e.message });
  }
};

export const updateUser = async (req, res) => {
  try {
    let data = req.body;
    if(req.body.password){
      return res.status(500).json({ error: true, message: "password can't be set explicitly" });
    }
    const userId = mongoose.Types.ObjectId(req.params.id);
    const result = await User.findByIdAndUpdate(userId, { $set: { ...data } }, { new: true, upsert: true }).populate('urls').populate('logs')
    return res.status(201).json({ error: false, message: 'user updated', data: result });
  } catch (e) {
    return res.status(500).json({ error: true, message: e.message });
  }
};

export const filterUser = async (req, res) => {
  try {
    const { start } = req.query;
    const data = req.body;
    const filters = Object.keys(data).map(key => {
      const temp = {};
      temp[`${key}`] = data[key];
      return temp;
    });
    let result = [];
    if(start){
      result = await User.find({ $or: { _id: { $gt: start } }, filters }).limit(10);
    } else {
      result = await User.find({ $or: filters }).limit(10);
    }
    const [next] = result.slice(-1);
    return res.status(200).json({ error: false, users: result, message: 'success', hasNext: !!next, next: next ? next._id : null });
  } catch (e) {
    return res.status(500).json({ error: true, message: e.message });
  }
};

export const changePass = async (req, res) => {
  try {
    console.log("passwrd")
    const {oldPass, newPass, userId} = req.body;
    const user = await User.findById(userId);
    const match = await bcrypt.compare(oldPass, user.password);
    if(!match){
    return res.status(401).json({ error: true, message: "password not match" });
    }
    const hash = await bcrypt.hash(newPass, 7);
    user.password = hash
    const token = signToken(user._id, hash, user.role);
    return res
      .status(200)
      .json({ error: false, message: "User deleted", data: await user.save(), token });
  } catch (e) {
    console.log(e)
    return res.status(500).json({ error: true, message: e.message });
  }
};