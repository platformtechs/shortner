import express from 'express';

import { createConnect } from './config/db';
import middlewareConfig from './config/middleware';
import { UserRoutes, UrlRoutes } from "./modules"; 

const app = express();

/*= ===Database config===*/
createConnect();

/*= ===Middleware config===*/
middlewareConfig(app);

/*= ===API Routes or urls===*/
app.use("/api", [UserRoutes]);
app.use(UrlRoutes);

module.exports = app;
