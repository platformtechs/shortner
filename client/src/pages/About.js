import React from "react";
import {Card} from 'reactstrap';
import TopNavBar from './Main/NavBar';

export default () => (
<>
    <TopNavBar />
    <div className="container">
        <Card>
        <h1 className="heading text-center">About</h1>
        <p>This is a sample url shortner program</p>
        </Card>
    </div>
</>
);
