import mongoose, { Schema } from 'mongoose';

const LogSchema = new Schema({
  _id: Schema.Types.ObjectId,
  userId: {
    type: Schema.Types.ObjectId, ref: 'User',
  },
  text: String,
  action: String,
}, { timestamps: true });

const Logs = mongoose.model('Logs', LogSchema);

export default Logs;