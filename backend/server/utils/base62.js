
export const encodeBase62 = (n) => {
    if (n === 0) {
        return '0';
    }
    let str = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
    let result = '';
    while (n > 0) {
        result = str[n % str.length] + result;
        n = parseInt(n / str.length, 10);
    }
    return result;
}

export const decodeBase62 = (s) => {
    let str = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
    let result = 0;
    for (let i = 0; i < s.length; i++) {
        let p = str.indexOf(s[i]);
        if (p < 0) {
            return NaN;
        }
        result += p * Math.pow(str.length, s.length - i - 1);
    }
    return result;
}

console.log(encodeBase62(12345))
console.log(decodeBase62("1w"))