/* eslint-disable one-var-declaration-per-line */
/* eslint-disable no-console */

process.env.NODE_ENV = 'test';

const expect = require('chai').expect;
const request = require('supertest');
const sinon = require('sinon');

const app = require('../../server/app');
const { createConnect, close } = require('../../server/config/db');

let verifyData;

describe('User Module', function () {
  this.timeout(100000);
  before((done) => {
    sinon.stub(console, 'log'); // disable console.log
    sinon.stub(console, 'info'); // disable console.info
    sinon.stub(console, 'warn'); // disable console.warn
    sinon.stub(console, 'error'); // disable console.error

    createConnect()
      .then(() => done())
      .catch(err => done(err));
  });

  // eslint-disable-next-line no-undef
  after((done) => {
    console.log.restore();
    console.info.restore();
    console.warn.restore();
    console.error.restore();

    close()
      .then(() => done())
      .catch(err => done(err));
  });

  it('OK, creating Admin works', (done) => {
    request(app).post('/api/auth/admin-register')
      .send({
        fullname: 'Admin',
        email: 'admin@ex.com',
        password: 'admin123',
      })
      .then(res => {
        const body = res.body;
        expect(body).to.contain.property('data');
        expect(body).to.contain.property('token');
        expect(body.data).to.contain.property('role');
        expect(body.data.role).equal('ADMIN');
        done();
      })
      .catch(err => done(err));
  });

  it('OK, creating a user works', (done) => {
    request(app).post('/api/user')
      .send({
        fullname: 'dilip kumar sharma',
        email: 'krdilip276@gmail.com',
        password: 'user123'
      })
      .then((res) => {
        const body = res.body;
        expect(body).to.contain.property('data');
        expect(body).to.contain.property('token');
        expect(body.data).to.contain.property('role');
        expect(body.data.role).equal('USER');
        done();
      })
      .catch(err => done(err));
  });
  it('OK, list user works', (done) => {
    request(app).get('/api/user')
      .then((res) => {
        expect(res.status).to.equal(200);
        const body = res.body;
        expect(body).to.contain.property('data');
        expect(body.data.length).equal(2)
        done();
      })
      .catch(err => done(err));
  });

});
