/* eslint-disable one-var-declaration-per-line */
/* eslint-disable no-console */

process.env.NODE_ENV = "test";

const expect = require("chai").expect;
const request = require("supertest");
const sinon = require("sinon");

const app = require("../../server/app");
const { createConnect, close } = require("../../server/config/db");

let userId = '';

describe("User Module", function () {
  this.timeout(100000);
  before((done) => {
    // sinon.stub(console, "log"); // disable console.log
    // sinon.stub(console, "info"); // disable console.info
    // sinon.stub(console, "warn"); // disable console.warn
    // sinon.stub(console, "error"); // disable console.error

    createConnect()
      .then(() => 
            request(app).post('/api/user')
            .send({
                fullname: 'dilip kumar sharma',
                email: 'krdilip123@gmail.com',
                password: 'user123'
            })
            .then((res) => {
                userId = res.body.data._id;
                done();
            })
            .catch(err => done(err))
      )
      .catch((err) => done(err));
  });

  // eslint-disable-next-line no-undef
  after((done) => {
    // console.log.restore();
    // console.info.restore();
    // console.warn.restore();
    // console.error.restore();

    close()
      .then(() => done())
      .catch((err) => done(err));
  });

  it("OK, creating url works", (done) => {
    request(app)
      .post("/api/u")
      .send({
        dUrl: 'https://platformtechs.in',
        userId
      })
      .then((res) => {
        const body = res.body;
        expect(body).to.contain.property("data");
        expect(body.data).to.contain.property("slug");
        expect(body.data.userId).equal(userId);
        done();
      })
      .catch((err) => done(err));
  });
});
