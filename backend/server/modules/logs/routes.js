import { Router } from 'express';
import * as UserController from './controller';
// import checkAuth from '../../config/checkAuth';

const routes = new Router();

routes.post('/auth/register', UserController.signup);
routes.post('/auth/admin-register', UserController.createAdmin);
// routes.post('/auth/verify-otp', UserController.verifyOtp);
// routes.get('/auth/login/:phone', UserController.login);
// routes.get('/user', UserController.getlistUser);
// routes.get('/user/number', UserController.partners);
// routes.get('/user/number/:id', UserController.numberUser);
// routes.get('/user/:id', UserController.getUser);
// routes.put('/user/:id', UserController.updateUser);
// routes.delete('/user/:id', UserController.deleteUser);
// routes.post('/user/filter', UserController.filterUser);
// routes.post('/user/:id/profile-image', UserController.uploadProfileImage);
// routes.post('/user/bank-details', UserController.paymentSetting);
// routes.post('/user/kyc', UserController.updateKyc);
// routes.get('/search-user/:search', UserController.searchUser);
// routes.get('/test', UserController.listner);

// routes.post('/user/changepass/:id', UserController.changePass);

export default routes;
