import { LOGIN, GET_USER, UPDATE_URL,} from './contants';

const initialState = {
    isLoggedIn : false,
    user: {},
    token: null,
    updateUrl:false
}

export default ( state=initialState, {type, payload}) => {
    switch (type) {
      case LOGIN: {
        let isLoggedIn = true;
        return {
          ...state,
          isLoggedIn,
          user: payload.user,
          token: payload.token,
        };
      }

      case GET_USER: {
        return {
          ...state,
          user: payload,
        };
      }
      case UPDATE_URL: {
        return {
          ...state,
          updateUrl: !state.updateUrl,
        };
      }

      default: {
        return state;
      }
    }
}

