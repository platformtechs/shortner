import React, {useState} from "react";
import { Form, Input, FormGroup, Label, Button, Card, Modal } from "reactstrap";
import Loader from 'react-loader-spinner';
import {Link} from 'react-router-dom';
import { Api } from "../../redux/api";
import {connect} from 'react-redux';
import { loginUser } from '../../redux/Auth/actions';

const Register = (props) => {
    const [fullname, setFullname] = useState();
    const [email, setEmail] = useState();
    const [password, setPassword] = useState();
    const [cnfPassword, setCnfPassword] = useState();
    const [loading, setLoading] = useState(false);

    const handleForm = (e) => {
      console.log("handle");
      e.preventDefault();
      e.stopPropagation();
      if(password !== cnfPassword) {
        return alert("password doesn't match");
      }
      setLoading(true);
      const data = {
        fullname,
        email,
        password,
      };
      Api.post("/api/user", data)
        .then((res) => {
          let resData = {token: res.data.token, user: res.data.data}; 
          props.loginUser(resData);
          setLoading(false);
        })
        .catch((err) => {console.log("loginError", err);setLoading(false); alert("login error");});
    };

  return (
    <div className="container">
      {loading && (
        <Modal isOpen centered style={{ backgroundColor: "transparent" }}>
          <div
            style={{
              justifyContent: "center",
              alignItems: "center",
              display: "flex",
              padding: 20,
              backgroundColor: "transparent",
            }}
          >
            <Loader type="BallTriangle" color="blue" height={200} width={200} />
          </div>
        </Modal>
      )}
      <Card style={{ padding: "150px", marginTop: "100px" }}>
        <h2 className="text-center"> Register</h2>
        <Form>
          <FormGroup>
            <Label for="fullameInput">Full name</Label>
            <Input
              onChange={(e) => setFullname(e.target.value)}
              type="text"
              name="fullname"
              id="fullnameInput"
              placeholder="Enter full name"
            />
          </FormGroup>
          <FormGroup>
            <Label for="emailInput">Email</Label>
            <Input
              onChange={(e) => setEmail(e.target.value)}
              type="email"
              name="email"
              id="emailInput"
              placeholder="Enter Email"
            />
          </FormGroup>
          <FormGroup>
            <Label for="passInput">Password</Label>
            <Input
              onChange={(e) => setPassword(e.target.value)}
              type="password"
              name="password"
              id="passInput"
              placeholder="Enter Password"
            />
          </FormGroup>
          <FormGroup>
            <Label for="cnfPassInput">Confirm Password</Label>
            <Input
              onChange={(e) => setCnfPassword(e.target.value)}
              type="password"
              name="cnfPassword"
              id="cnfPassInput"
              placeholder="Confirm Password"
            />
          </FormGroup>
          <Button color="primary" size="lg" block onClick={handleForm}>
            SUBMIT NOW
          </Button>
          <div style={{ marginTop: "20px", float: "right" }}>
            <Link to="/login">Already Account Login here</Link>
          </div>
        </Form>
      </Card>
    </div>
  );
};

export default connect(null, { loginUser })(Register);
