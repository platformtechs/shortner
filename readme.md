## Url Shortner

- ### Quick installation guide
  
#### prequisite: Node version : 12.x.x

1. clone the repository
2. go to *backend* folder
3. Run the command <br>
 `npm install` or `yarn install` in backend directory to install dependencies
4. run the command to start backend server
   `yarn start` or `npm install`
5. now open another terminal go to *client* folder
6. Run the command <br>
 `npm install` or `yarn install` in client directory to install dependencies
4. run the command to start client server
   `yarn start` or `npm install`
  

- ### More information
  1. backend and frontend are seperate directories which can be started separately
  2. to change configuation of backend you can check `.env` file and `/server/config` folder
  3. to change url endpoint in frontend go to `src/redux/api` file.
  4.  postman collection api [https://www.getpostman.com/collections/619d62195ea3f103bb92](https://www.getpostman.com/collections/619d62195ea3f103bb92)

- ### to run tests
  1. go to backend folder 
  2. run the command after all dependencies installation in quick guide `yarn run test`
- ### future scope
  1. social login
  2. separate admin login
  3. statics dashboard
  4. deletion of urls
  5. user specific slug creation
  6. profile update
  7. logs listing

## docker development server
- setup docker using link (if it is not available in your system) : https://docs.docker.com/compose/gettingstarted/
- run `docker-compose up --build`
- go to `http://localhost:3000`