import React, { Component } from "react";
import { Route, Switch, Redirect, BrowserRouter } from "react-router-dom";

//redux libraries
import { Provider } from "react-redux";
import { PersistGate } from "redux-persist/integration/react";
import { getStore, getPersistor, getState } from "./redux/store";

import 'bootstrap/dist/css/bootstrap.css';
// Put any other imports below so that CSS from your
// components takes precedence over default styles.
import "./App.css";

const loading = () => (
  <div className="animated fadeIn pt-3 text-center">Loading...</div>
);

// Pages
const HomePage = React.lazy(()=> import('./pages/Main'))
const Login = React.lazy(() => import("./pages/Auth/Login"));
const Register = React.lazy(() => import("./pages/Auth/Register"));
const Page404 = React.lazy(() => import("./pages/Page404"));
const About = React.lazy(()=> import("./pages/About"));
// redux configuration
const store = getStore();
const myPersistor = getPersistor();

class App extends Component {
  render() {
    return (
      <Provider store={store}>
        <PersistGate persistor={myPersistor} loading={null}>
          <BrowserRouter>
            <React.Suspense fallback={loading()}>
              <Switch>
                <Route
                  exact
                  path="/register"
                  name="Register Page"
                  render={(props) => <Register {...props} />}
                />
                <Route
                  exact
                  path="/login"
                  name="Login Page"
                  render={(props) => <Login {...props} />}
                />
                <Route
                  exact
                  path="/404"
                  name="Page 404"
                  render={(props) => <Page404 {...props} />}
                />
                <Route
                  exact
                  path="/about"
                  name="Page 404"
                  render={(props) => <About {...props} />}
                />
                <PrivateRoute
                  path="/"
                  name="Home"
                  component={(props) => <HomePage {...props} />}
                />
              </Switch>
            </React.Suspense>
          </BrowserRouter>
        </PersistGate>
      </Provider>
    );
  }
}

function PrivateRoute({ component: Component, ...rest }) {
  const state = getState();
  console.log("state", state);
  return (
    <Route
      {...rest}
      render={(props) =>
        state.auth.isLoggedIn ? (
          <Component {...props} />
        ) : (
          <Redirect
            to={{
              pathname: "/login",
              state: {
                from: props.location,
              },
            }}
          />
        )
      }
    />
  );
}

export default App;
