import mongoose from 'mongoose';
import * as fs from 'fs';
import * as path from 'path';
import Url from './model';
import {encodeBase62, decodeBase62} from '../../utils/base62';
import User from '../users/model';

export const createUrl = async (req, res) => {
  try {
    const {dUrl, userId} = req.body;
    const oldUrl = await User.findOne({ destination: dUrl });
    if(oldUrl){
      return res.status(201).json({ error: false, message: 'url is already available', data: oldUrl.slug });
    }
    const filePath = path.join(__dirname, './dataFile.json')
    const dataFile = fs.readFileSync(filePath);
    const data = await JSON.parse(dataFile);
    const slug = encodeBase62(data.slugId);
    const newUrl = new Url({
      _id: new mongoose.Types.ObjectId(),
      userId,
      destination: dUrl,
      slug
    });
    const savedUrl = await newUrl.save();
    data.slugId = data.slugId + 1;
    fs.writeFile(filePath, JSON.stringify(data), 'utf8', (err) => {
      if (err) throw err;
      console.log("The file has been saved!");
    });
    await User.findByIdAndUpdate(userId, {$push: {urls: savedUrl._id}})
    console.log("hostname", req.hostname);
    return res.status(200).json({ error: false, message: 'slug created', data: savedUrl});  
  } catch (e) {
    console.log(e);
    return res.status(500).json({ error: true, message: e.message });
  }
};

export const redirectUrl = async (req, res) => {
  try {
    const url = await Url.findOneAndUpdate(
      { slug: req.params.slug },
      { $inc: { clickCount : 1}}, { new: true, upsert: false });
    if (!url){
    return res.status(500).json({ error: true, message: "url doesn't exist" });
    }
    return res.redirect(url.destination)
  } catch (e) {
    return res.status(500).json({ error: true, message: e.message });
  }
};

export const urlStatics = async (req, res) => {
  try {
    const data = await Url.aggregate([
      {
        $set: {
          createdWeek: { $week: "$createdAt" },
          createdMonth: { $month: "$createdAt" },
          createdYear: { $year: "$createdAt" },
          createdDay: { $dayOfMonth: "$createdAt" },
        },
      },
      {
        $facet: {
          total: [
            {
              $group: {
                _id: null,
                count: { $sum: 1 },
                totalClick: { $sum: { $toInt: "$clickCount" } },
              },
            },
          ],
          weekly: [
            {
              $match: {
                $expr: {
                  $and: [
                    { $eq: ["$createdWeek", { $week: "$$NOW" }] },
                    { $eq: ["$createdYear", { $year: "$$NOW" }] },
                  ],
                },
              },
            },
            {
              $group: {
                _id: null,
                count: { $sum: 1 },
                totalClick: { $sum: { $toInt: "$clickCount" } },
              },
            },
          ],
          monthly: [
            {
              $match: {
                $expr: {
                  $and: [
                    { $eq: ["$createdMonth", { $month: "$$NOW" }] },
                    { $eq: ["$createdYear", { $year: "$$NOW" }] },
                  ],
                },
              },
            },
            {
              $group: {
                _id: null,
                count: { $sum: 1 },
                totalClick: { $sum: { $toInt: "$clickCount" } },
              },
            },
          ],
          today: [
            {
              $match: {
                $expr: {
                  $and: [
                    { $eq: ["$createdDay", { $dayOfMonth: "$$NOW" }] },
                    { $eq: ["$createdMonth", { $month: "$$NOW" }] },
                    { $eq: ["$createdYear", { $year: "$$NOW" }] },
                  ],
                },
              },
            },
            {
              $group: {
                _id: null,
                count: { $sum: 1 },
                totalClick: { $sum: { $toInt: "$clickCount" } },
              },
            },
          ],
        },
      },
    ]);
    return res
      .status(200)
      .json({ error: false, message: "slug created", data });  
  } catch (e) {
    return res.status(500).json({ error: true, message: e.message });
  }
};