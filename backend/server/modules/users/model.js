/* eslint-disable no-console */
import mongoose, { Schema } from 'mongoose';

const UserSchema = new Schema(
  {
    _id: Schema.Types.ObjectId,
    email: {
      type: String,
      index: 'user',
      unique: true,
      required: true,
    },
    password: String,
    role: {
      type: String,
      enum: ['ADMIN', 'USER'],
      required: true,
      index: 'role',
    },
    fullname: String,
    urls: [{
      type: Schema.Types.ObjectId, ref: 'Url',
    }],
    logs: [{
      type: Schema.Types.ObjectId, ref: 'Logs',
    }],
    isBlocked: {
      type: Boolean,
      default: false
    },
    lastLogin: Date
  }, { timestamps: true }
);


// indexes
UserSchema.index({ fullname: 'text', email: 'text' });

// model intialize
const User = mongoose.model('User', UserSchema);

export default User;