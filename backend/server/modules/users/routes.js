import { Router } from 'express';
import * as UserController from './controller';
import checkAuth from '../../config/checkAuth';

const routes = new Router();

routes.post('/user', UserController.createUser);
routes.post("/user/change-pass", checkAuth, UserController.changePass);
routes.post('/user/filter', UserController.filterUser);


routes.get('/user', UserController.getlistUser);
routes.get('/user/:id', checkAuth, UserController.getUser);

routes.put('/user/:id',checkAuth, UserController.updateUser);

routes.delete('/user/:id',checkAuth, UserController.deleteUser);


routes.post('/auth/admin-register', UserController.createAdmin);
routes.post('/auth/login', UserController.login);


export default routes;
