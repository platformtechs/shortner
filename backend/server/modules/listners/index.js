import mongoose from 'mongoose';
import { Logs, User } from '../users/model';
import Wallet from '../wallet/model';
import { Order } from '../orders/model';
import { roundAccurately } from '../../utils';

export default app => {
  app.on('test', newUserJoin);
  app.on('membershipUpgrade', membershipDistribution);
  app.on('membershipProduct', membershipProductDistribution);
};

const newUserJoin = async (data) => {
  try {
    console.log('hello', data.data);
  } catch (error) {
    console.log(error);
  }
};

// membership distribution

const membershipDistribution = async (data) => {
  try {
    const { userId, orderId, amount } = data;
    console.log('membership amount===>', amount);
    const amountNumber = Number(amount);
    const moneyBackEligibility = roundAccurately((amountNumber * 1.11), 2);
    await User.findByIdAndUpdate(userId, { $push: { orders: orderId } }, { new: true });
    await Wallet.findOneAndUpdate({ user: userId }, { $inc: { 'moneyBack.moneyBackEligibility': moneyBackEligibility, 'moneyBack.amountOfPurchase': amountNumber } }, { new: true });
    const [parents] = await User.aggregate([
      { $match: { _id: mongoose.Types.ObjectId(userId) } },
      { $graphLookup: {
        from: User.collection.name,
        startWith: '$parent',
        connectFromField: 'parent',
        connectToField: '_id',
        as: 'parents',
        maxDepth: 11,
        depthField: 'upLevel',
      } },
      // { $unwind: '$parents' },
      { $project: { parents: '$parents' } },
    ]);
    if (parents) {
      const distribution = [];
      parents.parents.forEach(async el => {
        const temp = el;
        console.log('el', el, el.membership);
        let tempDstribution = {};
        if (!el.membership) {
          await User.findByIdAndUpdate(el._id, { $set: { membership: 'MICRO' } }, { new: true });
          const wallet = await Wallet.findOneAndUpdate({ user: el._id }, { $inc: { 'moneyBack.total': 100, 'moneyBack.monthlyMoneyBack': 100, 'moneyBack.pending': 100 } }, { new: true, upsert: true });
          const transactions = {
            orderId,
            text: `Rs 100 is credited from circle ${el.upLevel + 1} and user ${userId}`,
            date: new Date(),
          };
          await Wallet.findByIdAndUpdate(wallet._id, { $push: { 'moneyBack.transactions': transactions } });
          const logs = new Logs({
            _id: mongoose.Types.ObjectId(),
            userId: el._id,
            action: 'MEMBERSHIP MONEYBACK',
            text: `Rs 100 is credited from circle ${el.upLevel + 1} and user ${userId}`,
          });
          await logs.save();
          const logUser = await User.findByIdAndUpdate(el._id, { $push: { logs: logs._id } }, { new: true });
          tempDstribution = {
            amount,
            userId: el._id,
            fcLevel: el.upLevel,
          };
          console.log('userData', logUser);
        } else if (el.membership === 'MICRO') {
          const wallet = await Wallet.findOneAndUpdate({ user: temp._id }, { $inc: { 'moneyBack.total': 100, 'moneyBack.monthlyMoneyBack': 100, 'moneyBack.pending': 100 } }, { new: true, upsert: true });
          const transactions = {
            orderId,
            text: `Rs 100 is credited from circle ${el.upLevel + 1} and user ${userId}`,
            date: new Date(),
          };
          await Wallet.findByIdAndUpdate(wallet._id, { $push: { 'moneyBack.transactions': transactions } });
          const logs = new Logs({
            _id: mongoose.Types.ObjectId(),
            userId: el._id,
            action: 'MEMBERSHIP MONEYBACK',
            text: `Rs 100 is credited from circle ${el.upLevel + 1} and user ${userId}`,
          });
          await logs.save();
          const logUser = await User.findByIdAndUpdate(el._id, { $push: { logs: logs._id } }, { new: true });
          tempDstribution = {
            amount,
            userId: el._id,
            fcLevel: el.upLevel,
          };
          console.log('logs', logUser);
        } else {
          const wallet = await Wallet.findOneAndUpdate({ user: temp._id }, { $inc: { 'moneyBack.$total': 100, 'moneyBack.$monthlyMoneyBack': 100, 'moneyBack.$pending': 100 } }, { new: true, upsert: true });
          const transactions = {
            orderId,
            text: `Rs 100 is credited from circle ${el.upLevel + 1} and user ${userId}`,
            date: new Date(),
          };
          await Wallet.findByIdAndUpdate(wallet._id, { $push: { 'moneyBack.transactions': transactions } });
          const logs = new Logs({
            _id: mongoose.Types.ObjectId(),
            userId: el._id,
            action: 'MEMBERSHIP MONEYBACK',
            text: `Rs 100 is credited from circle ${el.upLevel + 1} and user ${userId}`,
          });
          await logs.save();
          const logUser = await User.findByIdAndUpdate(el._id, { $push: { logs: logs._id } }, { new: true });
          tempDstribution = {
            amount,
            userId: el._id,
            fcLevel: el.upLevel,
          };
          console.log('logs', logUser);
        }
        const updateOrder = await Order.findByIdAndUpdate(orderId, { $push: { distribution: tempDstribution } }, { new: true });
        console.log('updatedOrder===>', updateOrder);
      });
    }
    return true;
  } catch (error) {
    console.log('err', error);
    return false;
  }
};

const membershipProductDistribution = async (data) => {
  try {
    const { userId, orderId } = data;
    const userData = await User.findByIdAndUpdate(userId, { $push: { orders: orderId } }, { new: true });
    const order = await Order.findById(orderId).populate('product');
    const cashBackUser = roundAccurately(Number(order.product.commission) * 0.35, 2);
    const moneyBack = roundAccurately((Number(order.product.commission) * 0.05), 2);
    const moneyBackEligibility = roundAccurately(Number(order.amount) * 1.11, 2);
    const amountOfPurchase = roundAccurately(Number(order.amount), 2);
    const updaateWallet = await Wallet.findOneAndUpdate({ user: userId }, { $inc: { 'cashBack.total': cashBackUser, 'cashBack.pending': cashBackUser, 'moneyBack.moneyBackEligibility': moneyBackEligibility, 'moneyBack.amountOfPurchase': amountOfPurchase } }, { new: true, upsert: true });
    console.log('wallet====>', updaateWallet);
    const [parents] = await User.aggregate([
      { $match: { _id: mongoose.Types.ObjectId(userId) } },
      { $graphLookup: {
        from: User.collection.name,
        startWith: '$parent',
        connectFromField: 'parent',
        connectToField: '_id',
        as: 'parents',
        maxDepth: 11,
        depthField: 'upLevel',
      } },
      // { $unwind: '$parents' },
      { $project: { parents: '$parents' } },
    ]);
    if (parents) {
      const distribution = [];
      parents.parents.forEach(async el => {
        const temp = el;
        // console.log('el', el, el.membership);
        let tempDstribution = {};
        if (!el.membership) {
          await User.findByIdAndUpdate(el._id, { $set: { membership: 'MICRO' } }, { new: true });
          const wallet = await Wallet.findOneAndUpdate({ user: el._id }, { $inc: { 'moneyBack.total': moneyBack, 'moneyBack.monthlyMoneyBack': moneyBack, 'moneyBack.pending': moneyBack } }, { new: true, upsert: true });
          console.log("wallet distri====>::", wallet);
          const transactions = {
            orderId,
            text: `Rs ${moneyBack} is credited from circle ${el.upLevel + 1} and user ${userId}`,
            date: new Date(),
          };
          await Wallet.findByIdAndUpdate(wallet._id, { $push: { 'moneyBack.transactions': transactions } });
          const logs = new Logs({
            _id: mongoose.Types.ObjectId(),
            userId: el._id,
            action: 'MEMBERSHIP PRODUCT',
            text: `Rs ${moneyBack} is credited from circle ${el.upLevel + 1} and user ${userId}`,
          });
          await logs.save();
          const logUser = await User.findByIdAndUpdate(el._id, { $push: { logs: logs._id } }, { new: true });
          tempDstribution = {
            amount: moneyBack,
            userId: el._id,
            fcLevel: el.upLevel,
          };
          console.log('userData', logUser);
        } else if (el.membership === 'MICRO') {
          const wallet = await Wallet.findOneAndUpdate({ user: temp._id }, { $inc: { 'moneyBack.total': moneyBack, 'moneyBack.monthlyMoneyBack': moneyBack, 'moneyBack.pending': moneyBack } }, { new: true, upsert: true });
          console.log("wallet distri====>::", wallet);
          const transactions = {
            orderId,
            text: `Rs ${moneyBack} is credited from circle ${el.upLevel + 1} and user ${userId}`,
            date: new Date(),
          };
          await Wallet.findByIdAndUpdate(wallet._id, { $push: { 'moneyBack.transactions': transactions } });
          const logs = new Logs({
            _id: mongoose.Types.ObjectId(),
            userId: el._id,
            action: 'MEMBERSHIP PRODUCT',
            text: `Rs ${moneyBack} is credited from circle ${el.upLevel + 1} and user ${userId}`,
          });
          await logs.save();
          const logUser = await User.findByIdAndUpdate(el._id, { $push: { logs: logs._id } }, { new: true });
          tempDstribution = {
            amount: moneyBack,
            userId: el._id,
            fcLevel: el.upLevel,
          };
          console.log('logs', logUser);
        } else {
          const wallet = await Wallet.findOneAndUpdate({ user: temp._id }, { $inc: { 'moneyBack.total': moneyBack, 'moneyBack.monthlyMoneyBack': moneyBack, 'moneyBack.pending': moneyBack } }, { new: true, upsert: true });
          console.log("wallet distri====>::", wallet);
          const transactions = {
            orderId,
            text: `Rs ${moneyBack} is credited from circle ${el.upLevel + 1} and user ${userId}`,
            date: new Date(),
          };
          await Wallet.findByIdAndUpdate(wallet._id, { $push: { 'moneyBack.transactions': transactions } });
          const logs = new Logs({
            _id: mongoose.Types.ObjectId(),
            userId: el._id,
            action: 'MEMBERSHIP PRODUCT',
            text: `Rs ${moneyBack} is credited from circle ${el.upLevel + 1} and user ${userId}`,
          });
          await logs.save();
          const logUser = await User.findByIdAndUpdate(el._id, { $push: { logs: logs._id } }, { new: true });
          tempDstribution = {
            amount: moneyBack,
            userId: el._id,
            fcLevel: el.upLevel,
          };
          console.log('logs', logUser);
        }
        const updateOrder = await Order.findByIdAndUpdate(orderId, { $push: { distribution: tempDstribution } }, { new: true });
        console.log('order', updateOrder);
      });
    }
    console.log('parents', userData);
    return true;
  } catch (error) {
    console.log('err', error);
    return false;
  }
};

