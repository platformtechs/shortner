import mongoose, { Schema } from 'mongoose';

const UrlSchema = new Schema({
  _id: Schema.Types.ObjectId,
  userId: {
    type: Schema.Types.ObjectId, ref: 'User',
  },
  destination: String,
  slug: {
    type: String,
    unique: true,
    index: 'urlIndex'
  },
  clickCount: {
    type: Number,
    default: 0
  },
}, { timestamps: true });

const SettingSchema = new Schema(
  {
    _id: Schema.Types.ObjectId,
    slugId: {
      type: Number,
      default: 0
    },
  },
  { timestamps: true }
);

const Url = mongoose.model('Url', UrlSchema);
const Setting = mongoose.model("Setting", SettingSchema);
export {
  Setting
};
export default Url;