import jwt, { decode } from 'jsonwebtoken';
import User from '../modules/users/model';
import bcrypt from 'bcrypt';

export default async (req, res, next) => {
  try {
    const token = req.headers.authorization.split(' ')[1];
    const decoded = jwt.verify(token, process.env.JWT_KEY);
    const user = await User.findById(decoded.id);
    const match = decoded.password === user.password
    if(!match){
      return res.status(401).json({
        error: true,
        message: "Auth failed",
      });
    }
    next();
  } catch (error) {
    return res.status(401).json({
      error: true,
      message: 'Auth failed',
    });
  }
};
