import { LOGIN } from './contants'
import {Api} from '../api';

export const loginUser = (data) => ({
    type: LOGIN,
    payload: data
})