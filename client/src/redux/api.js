import axios from "axios";


// export const baseUrl = "http://192.168.1.7:3001";
export const baseUrl = "http://localhost:3001";

// export const baseUrl = 'https://zoogol.herokuapp.com';

export const Api = axios.create({
  baseURL: baseUrl,
  // headers: {
  //     'content-type': 'application/json'
  // }
});
