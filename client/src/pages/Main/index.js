import React, {useState} from "react";
import {
  Form,
  Input,
  FormGroup,
  Label,
  Button,
  Card,
  Modal,
  Col,
  Row,
  Table,
  InputGroup,
  InputGroupAddon,
} from "reactstrap";
import Loader from "react-loader-spinner";
import UrlShortner from './urlShortner'
import TopNavBar from "./NavBar";
import Profile from "./profile";
import ChangePassword from './changePass';

export default (props) => {
  return (
    <>
      <TopNavBar />
      <Row>
        <Col md="6">
          <Profile />
        </Col>
        <Col md="6">
          <UrlShortner />
          <ChangePassword />
        </Col>
      </Row>
    </>
  );};
