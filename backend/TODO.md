### To do list
- [x] environment setup
- [x] schema design
- [x] user profile
- [x] link shortening
- [ ] social auth
- [x] link statics