import React, {useEffect} from 'react'
import {Card, Table} from 'reactstrap';
import DataTable, { createTheme } from "react-data-table-component";
import {useHistory} from 'react-router-dom';
import * as moment from 'moment';
import {connect} from 'react-redux';
import { Api, baseUrl } from '../../redux/api';
import {getUser, logout} from '../../redux/Auth/actions';
const Profile = (props) => {
    const {fullname, email, urls} = props.user;
    const history = useHistory();
    useEffect(() => {
        Api.get(`/api/user/${props.user._id}`, {
          headers: {
            authorization: `Bearer ${props.token}`,
          },
        })
          .then((res) => {
            props.getUser(res.data.data);
          })
          .catch((err) => {
              if(err.response && err.response.status == 401) {
                  alert("session out");
                  props.logout();
                  history.push('/login');
                  // window.location.reload();
              } else {
                alert("something went wrong");
              }
          });
    }, [props.update]);

    const data =
      urls &&
      urls.map((item, index) => {
        return {
          sno: index+1,
          id: item._id,
          destination: item.destination || "NA",
          slug: baseUrl + "/u/" + item.slug || "NA",
          clickCount: item.clickCount || "NA",
          createdOn: moment(item.createdAt).format("LL"),
          lastVisited: moment(item.updatedAt).format("LL"),
        };
      });

      const columns = [
        {
          name: "S. NO.",
          selector: "sno",
          maxWidth: "30px",
          sortable: true,
        },
        {
          name: "Main Url",
          selector: "destination",
          sortable: true,
          cell: (row) => (
            <a href={row.destination} target="_blank">
              {row.destination}
            </a>
          ),
        },
        {
          name: "Short Url",
          selector: "slug",
          sortable: true,
          cell: (row) => (
            <a href={row.slug} target="_blank">
              {row.slug}
            </a>
          ),
        },
        {
          name: "Created On",
          selector: "createdOn",
          sortable: true,
        },
        {
          name: "last visited",
          selector: "lastVisited",
          sortable: true,
        },
      ];
  return (
    <Card style={{ padding: "50px" }}>
      <h3>Profile Details</h3>
      <Table style={{ marginTop: "50px" }}>
        <thead></thead>
        <tbody>
          <tr>
            <td>Name</td>
            <td>{fullname}</td>
          </tr>
          <tr>
            <td>Email</td>
            <td>{email}</td>
          </tr>
          <tr>
            <td>total links</td>
            <td>{urls && urls.length}</td>
          </tr>
        </tbody>
      </Table>
      <DataTable
        title="Links"
        data={data}
        // selectableRows
        columns={columns}
        pagination={true}
        paginationPerPage={10}
        responsive={true}
      />
    </Card>
  );
};
const mapStateToProps = (state) => ({
  user: state.auth.user,
  update: state.auth.updateUrl,
  token: state.auth.token
});
export default connect(mapStateToProps, { getUser, logout })(Profile);
