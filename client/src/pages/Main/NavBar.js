import React, { useState } from "react";
import {
  Collapse,
  Navbar,
  NavbarToggler,
  NavbarBrand,
  Nav,
  NavItem,
  NavLink,
  UncontrolledDropdown,
  DropdownToggle,
  DropdownMenu,
  DropdownItem,
  NavbarText,
  Button,
} from "reactstrap";
import {connect} from 'react-redux';
import {logout} from '../../redux/Auth/actions'
import { useHistory } from "react-router-dom";

const TopNavBar = (props) => {
    const history = useHistory();
  const [isOpen, setIsOpen] = useState(false);

  const toggle = () => setIsOpen(!isOpen);
  const handleLogout = () => {
    props.logout();
    history.push("/login");
  }
  return (
    <div>
      <Navbar color="red" light expand="md">
        <NavbarBrand href="/">URL SHORTNER</NavbarBrand>
        <NavbarToggler onClick={toggle} />
        <Collapse isOpen={isOpen} navbar>
          <Nav className="mr-auto" navbar>
            <NavItem>
              <NavLink href="/about">About</NavLink>
            </NavItem>
          </Nav>
          <Nav className="ml-auto" navbar>
            <NavItem className="navbar-right">
              <Button color="primary" block onClick={handleLogout}>Logout</Button>
            </NavItem>
          </Nav>
        </Collapse>
      </Navbar>
    </div>
  );
};

export default connect(null, {logout})(TopNavBar);
