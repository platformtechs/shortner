import mongoose from 'mongoose';
import bcrypt from 'bcrypt';
import jwt from 'jsonwebtoken';

const signToken = (id, phone, role) => jwt.sign({ id, role, phone }, JWT_SECRETE, { expiresIn: '7d' });

export const createAdmin = async (req, res) => {
  try {
    const { fullname, email, phone } = req.body;
    const oldUser = await User.findOne({ phone });
    if(oldUser){
      return res.status(403).json({ error: true, message: 'user already exist' });
    }
    const newUser = new User({
      _id: new mongoose.Types.ObjectId(),
      fullname,
      email,
      phone,
      isActive: true,
      role: 'ADMIN',
    });
    const token = signToken(newUser._id, phone, newUser.role);
    return res.status(200).json({ error: false, message: 'user created', data: await newUser.save(), token });  
  } catch (e) {
    console.log(e);
    return res.status(500).json({ error: true, message: e.message });
  }
};

export const signup = async (req, res) => {  
  try {
    const { fullname, email, phone } = req.body;
    const oldUser = await User.findOne({ phone });
    if(oldUser){
      return res.status(403).json({ error: true, message: 'user already exist' });
    }
    const newUser = new User({
      _id: new mongoose.Types.ObjectId(),
      fullname,
      email,
      phone,
      role: 'USER',
    });
    // otp
    const otp = createOtp();
    const text = `Use the ${otp} as OTP to verify your account on cabaret app. if you haven't register. ignore the mail.  --thanks`;
    const sub = 'cabaret:verify account';
    const newOtp = new OTP({
      _id: new mongoose.Types.ObjectId(),
      userId: newUser._id,
      otp,
    });
    sendMail(email, sub, text);
    sendSms(phone, otp);
    const logs = new Logs({
      _id: new mongoose.Types.ObjectId(),
      userId: newUser._id,
      action: 'user created',
      text: `user created on date ${moment().format('LL')}`,
    });
    newUser.logs = [logs._id];
    const NewUser = await newUser.save();
    const NewOtp = await newOtp.save();
    const Log = await logs.save();

    const result = {
      otp: NewOtp.otp,
      userId: NewUser._id,
    };
    return res.status(200).json({ error: false, message: Log, data: result });
  } catch (e) {
    console.log('err->signup:', e);
    return res.status(500).json({ error: true, message: e.message });
  }
};

export const verifyOtp = async (req, res) => {
  try {
    const { otp, userId } = req.body;
    const user = await User.findById(userId)
      .populate({ path: 'orders', populate: [{ path: 'meals', populate: { path: 'product' } }, { path: 'store' }] })
      .populate('logs');
    if(!user){
      return res.status(404).json({ error: false, message: 'user not found' });  
    }
    const otpData = await OTP.findOne({ userId });
    if(!otp){
      return res.status(404).json({ error: false, message: 'Otp expired' });  
    }
    if(otp === otpData.otp){
      user.isVerified = true;
      user.lastLogin = Date.now();
      const token = signToken(user._id, user.phone, user.role);
      const saveUser = await user.save();
      const responseData = {
        user: saveUser,
        token,
      };
      return res.status(200).json({ error: false, message: 'Otp verified', data: responseData });
    }
    return res.status(403).json({ error: true, message: 'Otp verification failed' });
  } catch (e) {
    return res.status(500).json({ error: true, message: e.message });
  }
};

export const login = async (req, res) => {
  try {
    const user = await User.findOne({ phone: req.params.phone });
    if(!user){
      return res.status(404).json({ error: true, message: 'User not exist' });  
    }
    // otp
    const otp = createOtp();
    const text = `Use the ${otp} as OTP to verify your account on cabaret app. if you haven't register. ignore the mail.  --thanks`;
    const sub = 'cabaret:verify account';
    const otpData = await OTP.findOne({ userId: user._id });
    if(otpData){
      const newText = `Use the ${otpData.otp} as OTP to verify your account on cabaret app. if you haven't register. ignore the mail.  --thanks`;
      sendMail(user.email, sub, newText);
      sendSms(user.phone, otpData.otp);
      const result = {
        otp: otpData.otp,
        userId: user._id,
      };
      return res.status(200).json({ error: false, message: 'Otp Sent', data: result });
    }

    const newOtp = new OTP({
      _id: new mongoose.Types.ObjectId(),
      userId: user._id,
      otp,
    });
    sendMail(user.email, sub, text);
    sendSms(user.phone, otp);
    const NewOtp = await newOtp.save();
    const result = {
      otp: NewOtp.otp,
      userId: user._id,
    };
    return res.status(200).json({ error: false, message: 'Otp Sent', data: result });
  } catch (e) {
    return res.status(500).json({ error: true, message: e.message });
  }
};

export const getUser = async (req, res) => {
  try {
    const userId = mongoose.Types.ObjectId(req.params.id);
    return res.status(201).json({ error: false,
      message: 'user found',
      data: await User.findById(userId)
        .populate({ path: 'orders', populate: [{ path: 'meals', populate: { path: 'product' } }, { path: 'store' }] })
        .populate('logs') });
  } catch (e) {
    return res.status(500).json({ error: true, message: e.message });
  }
};

export const getlistUser = async (req, res) => {
  try {
    const { start } = req.query;
    let result = [];
    if(start){
      result = await User.find({ _id: { $gt: start } }).limit(10);
    } else {
      result = await User.find().limit(10);
    }
    const [next] = result.slice(-1);
    return res.status(200).json({ error: false, message: 'User list found', data: result, hasNext: !!next, next: next ? next._id : null });
  } catch (e) {
    return res.status(500).json({ error: true, message: e.message });
  }
};

export const deleteUser = async (req, res) => {
  try {
    const userId = mongoose.Types.ObjectId(req.params.id);
    const result = await User.findByIdAndDelete(userId);
    return res.status(200).json({ error: false, message: 'User deleted', data: result });
  } catch (e) {
    return res.status(500).json({ error: true, message: e.message });
  }
};

export const updateUser = async (req, res) => {
  try {
    let data = req.body;
    console.log('files', req.file, req.files);
    console.log('re===>', req.body);

    if(req.file){
      data = { ...req.data, proofUrl: req.file.location };
    }
    if(req.body.password){
      return res.status(500).json({ error: true, message: "password can't be set explicitly" });
    }
    const userId = mongoose.Types.ObjectId(req.params.id);
    const result = await User.findByIdAndUpdate(userId, { $set: { ...data } }, { new: true, upsert: true })
      .populate('orders')
      .populate('logs');
    return res.status(201).json({ error: false, message: 'user updated', data: result });
  } catch (e) {
    return res.status(500).json({ error: true, message: e.message });
  }
};

export const filterUser = async (req, res) => {
  try {
    const { start } = req.query;
    const data = req.body;
    const filters = Object.keys(data).map(key => {
      const temp = {};
      temp[`${key}`] = data[key];
      return temp;
    });
    let result = [];
    if(start){
      result = await User.find({ $or: { _id: { $gt: start } }, filters }).limit(10);
    } else {
      result = await User.find({ $or: filters }).limit(10);
    }
    const [next] = result.slice(-1);
    return res.status(200).json({ error: false, users: result, message: 'success', hasNext: !!next, next: next ? next._id : null });
  } catch (e) {
    return res.status(500).json({ error: true, message: e.message });
  }
};

export const uploadProfileImage = async (req, res) => {
  try {
    console.log('file path', req.file.location);
    const url = `${req.file.location}`;
    const result = await User.findByIdAndUpdate(req.params.id, { $set: { imageUrl: url } }, { new: true, useFindAndModify: false });
    return res.status(200).json({ error: false, message: 'success', data: result });
  } catch (e) {
    return res.status(500).json({ error: true, message: e.message });
  }
};

export const paymentSetting = async (req, res) => {
  try {
    const { otp, userId, bankName, accHolderName, branch, accNo, ifsc } = req.body;
    const user = await User.findById(userId);
    if(!user){
      return res.status(404).json({ error: false, message: 'usr not found' });  
    }
    const otpData = await OTP.findOne({ userId });
    if(!otp){
      return res.status(404).json({ error: false, message: 'Otp expired' });  
    }
    if(otp === otpData.otp){
      const bank = await Bank.findOneAndUpdate({ userId }, { $set: { bankName, accHolderName, branch, accNo, ifsc } }, { new: true, upsert: true });
      user.payment = bank._id;
      user.save();
      return res.status(200).json({ error: false, message: 'bank details updated', data: bank });
    }
    return res.status(501).json({ error: true, message: 'system error' });
  } catch (e) {
    return res.status(500).json({ error: true, message: e.message });
  }
};

export const updateKyc = async (req, res) => {
  try {
    const { userId, pan, aadhar, nominee, age, relation } = req.body;
    const user = await User.findById(userId);
    if(!user){
      return res.status(404).json({ error: false, message: 'usr not found' });  
    }
    const kyc = await KYC.findOneAndUpdate({ userId }, { $set: { pan, aadhar, nominee, age, relation } }, { new: true, upsert: true });
    user.kyc = kyc._id;
    user.save();
    return res.status(200).json({ error: false, message: 'kyc details updated', data: kyc });
  } catch (e) {
    return res.status(500).json({ error: true, message: e.message });
  }
};

// export const getUserSummary = await 
export const partners = async (req, res) => {
  try {
    const data = await User.aggregate([{
      $facet: {
        UserActivity: [
          // { $match: { lastTransaction: !null } },
          { $group: { _id: '$lastTransaction', count: { $sum: 1 } } }],
        Membership: [
          // { $match: { lastTransaction: !null } },
          { $group: { _id: '$membership', count: { $sum: 1 } } }],
      },
    }]);
    // const data = await User.countDocuments({});
    // const data = await User.aggregate([
    //   { $match: {} },
    //   { $group: { _id: null, n: { $sum: 1 } } },
    // ]);
    return res.status(200).json({ error: false, message: 'data found', data });
  } catch (e) {
    return res.status(500).json({ error: true, message: e.message });
  }
};

export const numberUser = async (req, res) => {
  try {
    console.log('params', req.params.id);
    // const user = await User.findById(req.params.id);
    // const ids = user.children.map(id => mongoose.Types.ObjectId(id));
    // const data = await User.aggregate([
    //   { $match: { _id: { $in: ids } } },
    //   // { $lookup:
    //   //   {
    //   //     from: 'User',
    //   //     localField: 'children',
    //   //     foreignField: '_id',
    //   //     as: 'docs',
    //   //   },
    //   // },
    //   // { $unwind: '$docs' },
    //   {
    //     $facet: {
    //       UserActivity: [
    //         { $group: { _id: '$lastTransaction', count: { $sum: 1 } } },
    //       ],
    //       membership: [
    //         { $group: { _id: '$membership', count: { $sum: 1 } } },
    //       ],
    //     },
    //   },
    // ]
    // );
    const data = await User.aggregate([
      { $match: { _id: mongoose.Types.ObjectId(req.params.id) } },
      { $graphLookup: {
        from: User.collection.name,
        startWith: '$children',
        connectFromField: 'children',
        connectToField: '_id',
        as: 'partners',
        maxDepth: 11,
        depthField: 'level',
      } },
      { $unwind: '$partners' },
      {
        $facet: {
          userActivity: [
            { $group: { _id: '$partners.isActive', count: { $sum: 1 } } },
          ],
          circle: [
            { $group: { _id: '$partners.level', count: { $sum: 1 } } },
          ],
          membership: [
            { $group: { _id: '$partners.membership', count: { $sum: 1 } } },
          ],
          circleWiseActivity: [
            { $group: { _id: { level: '$partners.level', active: '$partners.isActive' }, count: { $sum: 1 } } },
          ],
          circleWiseMembership: [
            { $group: { _id: { level: '$partners.level', active: '$partners.membership' }, count: { $sum: 1 } } },
          ],
        } },
      
    ]);
    return res.status(200).json({ error: false, message: 'data found', data });
  } catch (e) {
    return res.status(500).json({ error: true, message: e.message });
  }
};

export const searchUser = async (req, res) => {
  try {
    const data = await User.find(
      { $text: { $search: req.params.search } },
      { score: { $meta: 'textScore' } }
    ).sort({ score: { $meta: 'textScore' } });
    console.log('search data', data);
    return res.status(200).json({ error: false, message: 'data found', data });
  } catch (e) {
    return res.status(500).json({ error: true, message: e.message });
  }
};

export const listner = async (req, res) => {
  try {
    res.app.emit('test', { data: 'hi' });
    return res.status(200).json({ error: false, message: 'data found' });
  } catch (e) {
    return res.status(500).json({ error: true, message: e.message });
  }
};
