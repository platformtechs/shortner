import React, { useState } from "react";
import {
  Form,
  Input,
  FormGroup,
  Label,
  Button,
  Card,
  Modal,
  Col,
  Row,
  Table,
  InputGroup,
  InputGroupAddon,
} from "reactstrap";
import Loader from "react-loader-spinner";
import { Api, baseUrl } from "../../redux/api";
import { useHistory, Link } from "react-router-dom";
import { CopyToClipboard } from "react-copy-to-clipboard";
import { connect } from "react-redux";
import { loginUser } from "../../redux/Auth/actions";

const ChangePassword = (props) => {
  const history = useHistory();
  const [oldPass, setOldPass] = useState("");
  const [loading, setLoading] = useState(false);
  const [newPass, setNewPass] = useState("");
  const [cnfPass, setCnfPass] = useState('');

  const handleForm = (e) => {
    console.log("handle");
    e.preventDefault();
    e.stopPropagation();
    if (newPass.trim().length < 3) {
      return alert("enter new password more than 3 characters");
    }
    if (newPass !== cnfPass ) {
      return alert("password doesn't match");
    }
    setLoading(true);
    const data = {
      oldPass,
      userId: props.user._id,
      newPass
    };
    Api.post(`/api/user/change-pass`, data, {
      headers: {
        authorization: `Bearer ${props.token}`,
      },
    })
      .then((res) => {
        let resData = { token: res.data.token, user: res.data.data };
        props.loginUser(resData);
        setLoading(false);
        window.reload();
      })
      .catch((err) => {
        console.log("loginError", err);
        setLoading(false);
        alert("something wrong try again");
      });
  };
  return (
    <Card style={{ padding: "50px" }}>
      {loading && (
        <Modal isOpen centered style={{ backgroundColor: "transparent" }}>
          <div
            style={{
              justifyContent: "center",
              alignItems: "center",
              display: "flex",
              padding: 20,
              backgroundColor: "transparent",
            }}
          >
            <Loader type="BallTriangle" color="blue" height={200} width={200} />
          </div>
        </Modal>
      )}
      <h2 className="text-center"> Change password to reset tokens</h2>
      <Form>
        <FormGroup>
          <Label for="oldPassInput">Enter Old password</Label>
          <Input
            onChange={(e) => setOldPass(e.target.value)}
            type="password"
            name="oldPass"
            id="oldPassInput"
            placeholder="Enter old password"
          />
        </FormGroup>
        <FormGroup>
          <Label for="newPassInput">Enter New password</Label>
          <Input
            onChange={(e) => setNewPass(e.target.value)}
            type="password"
            name="newPass"
            id="newPassInput"
            placeholder="Enter new password"
          />
        </FormGroup>
        <FormGroup>
          <Label for="cnfPassInput">Confirm password</Label>
          <Input
            onChange={(e) => setCnfPass(e.target.value)}
            type="password"
            name="oldPass"
            id="cnfPassInput"
            placeholder="confirm password"
          />
        </FormGroup>
        <Button
          type="button"
          color="primary"
          size="lg"
          block
          onClick={handleForm}
        >
          CHANGE NOW
        </Button>
        {/* <div style={{ marginTop: "20px", float: "right" }}>
            <Link to="/register">New User Login Here</Link>
          </div> */}
      </Form>
    </Card>
  );
};
const mapStateToProps = (state) => ({
  user: state.auth.user,
  token: state.auth.token,
});
export default connect(mapStateToProps, { loginUser })(ChangePassword);
