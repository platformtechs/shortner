import { LOGIN, GET_USER, UPDATE_URL } from './contants'
import {Api} from '../api';

export const loginUser = (data) => ({
    type: LOGIN,
    payload: data
})

export const getUser = (data) => ({
  type: GET_USER,
  payload: data,
});

export const updateUrl = () => ({
    type: UPDATE_URL,
    payload: null
});

export const logout = () => ({
    type:'RESET',
    payload: null
})
