import React, { useState } from "react";
import {
  Form,
  Input,
  FormGroup,
  Label,
  Button,
  Card,
  Modal,
  Col,
  Row,
  Table,
  InputGroup,
  InputGroupAddon,
} from "reactstrap";
import Loader from "react-loader-spinner";
import { Api, baseUrl } from "../../redux/api";
import { useHistory, Link } from "react-router-dom";
import { CopyToClipboard } from "react-copy-to-clipboard";
import {connect} from 'react-redux';
import {updateUrl} from '../../redux/Auth/actions';

const UrlShortner = (props) => {
    const history = useHistory();
    const [dUrl, setUrl] = useState('');
    const [loading, setLoading] = useState(false);
    const [sUrl, setSurl] = useState("");
    const [copied, setCopied] = useState(false)

    const handleForm = (e) => {
      console.log("handle");
      e.preventDefault();
      e.stopPropagation();
      if(dUrl.trim().length < 1){
          return alert("enter the url");
      }
      const urlRegx =
        /[(http(s)?)://(www.)?a-zA-Z0-9@:%._+~#=]{2,256}.[a-z]{2,6}\b([-a-zA-Z0-9@:%_+.~#?&//=]*)/;
        if(!dUrl.match(urlRegx)){
            return alert("enter the valid url");
        }
        setLoading(true);
      const data = {
          dUrl,
          userId: props.user._id
        // email, password
      };
      Api.post("/u", data)
        .then((res) => {
            console.log("urlres", res);
            setSurl(`${baseUrl}/u/${res.data.data.slug}`)
            props.updateUrl();
        //   let resData = { token: res.data.token, user: res.data.data };
        //   props.loginUser(resData);
          setLoading(false);
        //   history.push("/");
        })
        .catch((err) => {
          console.log("loginError", err);
          setLoading(false);
          alert("something wrong try again")
        });
    };
    return (
      <Card style={{ padding: "50px" }}>
        {loading && (
          <Modal isOpen centered style={{ backgroundColor: "transparent" }}>
            <div
              style={{
                justifyContent: "center",
                alignItems: "center",
                display: "flex",
                padding: 20,
                backgroundColor: "transparent",
              }}
            >
              <Loader
                type="BallTriangle"
                color="blue"
                height={200}
                width={200}
              />
            </div>
          </Modal>
        )}
        <h2 className="text-center"> Get Short Url</h2>
        <Form>
          <FormGroup>
            <Label for="emailInput">Enter your url</Label>
            <Input
              onChange={(e) => setUrl(e.target.value)}
              type="url"
              name="url"
              id="urlInput"
              placeholder="Enter Url"
            />
          </FormGroup>
          <FormGroup>
            <InputGroup>
              <Input
                disabled
                type="url"
                name="shortUrl"
                id="shortUrlInput"
                value={sUrl}
              />
              <InputGroupAddon addonType="append">
                <CopyToClipboard
                  text={sUrl}
                  onCopy={() => setCopied({ copied: true })}
                >
                  <Button color="primary" onClick={(e)=> e.preventDefault()}>{copied? 'copied': 'click to copy'}</Button>
                </CopyToClipboard>
              </InputGroupAddon>
            </InputGroup>
          </FormGroup>
          <Button
            type="button"
            color="primary"
            size="lg"
            block
            onClick={handleForm}
          >
            {sUrl ? "Create Another" : "Create Now"}
          </Button>
          {/* <div style={{ marginTop: "20px", float: "right" }}>
            <Link to="/register">New User Login Here</Link>
          </div> */}
        </Form>
      </Card>
    );
};
const mapStateToProps=(state) => ({
    user: state.auth.user,
    token: state.auth.token,
})
export default connect(mapStateToProps, { updateUrl })(UrlShortner);